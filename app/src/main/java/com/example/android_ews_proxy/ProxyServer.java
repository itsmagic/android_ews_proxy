package com.example.android_ews_proxy;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.StrictHostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import javax.net.ssl.SSLContext;

import fi.iki.elonen.NanoHTTPD;

public class ProxyServer extends NanoHTTPD {
    
    private JSONObject token;
    private Date received;
    private String clientId;
    private String clientSecret;
    private String tenantId;
    private String panelUUID;
    
    public ProxyServer() {
        super(9000);
    }

    @Override
    public Response serve(IHTTPSession session) {
        /*
         TODO We need to check who is connecting -- we could give the panel a cookie and check it
              as once we are authenticated anyone can use the proxy... until the token expires
              Also we would not be able to tell if they changed username / password
        */
        String clientIp = session.getHeaders().get("http-client-ip");
        Log.w("ProxyServer", "Client IP: " + clientIp);
        CookieHandler cookies = new CookieHandler(session.getHeaders());
        if (clientIp != "127.0.0.1"){
            //Do not allow connections outside of the panel/localhost -- this isn't secure, as another proxy running on the panel would have a localhost address
            Log.w("ProxyServer", "You cannot connect to this proxy from any outside address");
            return newFixedLengthResponse(Response.Status.FORBIDDEN, "text/html", "No access from external IP Addresses");
        }
        Log.w("ProxyServer", "Start of handling a request");
        /*
         IF we don't parseBody -- nanohttpd will not handle the next request.
         So do this before returning anything
        */
        Map<String, String> files = new HashMap<String, String>();
        try {
            session.parseBody(files);
        } catch (IOException e) {
            Log.w("ProxyServer", "Got an IOException: " + e);
        } catch (ResponseException e) {
            Log.w("ProxyServer", "Got a Response Exception: " + e);
        }
        String originalPostData = files.get("postData");

        try {
            /*
             The uri should be /ews/<tenantId>
             If this fails we should just bail
            */
            tenantId = session.getUri().substring(5).trim();
            Log.w("ProxyServer", "TenantId: " + tenantId);
        } catch (Exception e) {
            return newFixedLengthResponse("Incorrect uri format, should be \"/ews/your-tenant-id\"");
        }

        //First see if we already have a token
        if (checkToken()){
            Log.w("ProxyServer", "We have a valid token..");
        }else{
            // checkToken is false -- so we need a token, so lets get the client id and secret
            String authorization = session.getHeaders().get("authorization");
            if (authorization != null && authorization.toLowerCase().startsWith("basic")) {
                String[] values = parseBasicAuth(authorization);
                clientId = values[0];
                clientSecret = values[1];
                getToken();
                if (!checkToken()) {
                    Log.w("ProxyServer", "Sending a 401 again");
                    return create401();
                }
            } else {
                Log.w("ProxyServer", "Sending a 401");
                Response response = create401();
//                panelUUID = null;
//                panelUUID = UUID.randomUUID().toString();
//                Cookie panelCookie = new Cookie("EWSProxy", panelUUID, 30);
//                cookies.set(panelCookie);
//                cookies.unloadQueue(response);

                return response;
            }
        }

        Method method = session.getMethod();
        if (Method.PUT.equals(method) || Method.POST.equals(method)) {

            Log.w("ProxyServer", "Processing a POST");
            try {
                String microsoftResponse = sendDataToMicrosoft(originalPostData);
                return newFixedLengthResponse(NanoHTTPD.Response.Status.OK, "text/xml; charset=utf-8", microsoftResponse);

            } catch (Exception e) {

                Log.w("ProxyServer", "Error making response: " + e);
                return newFixedLengthResponse("Error");
            }
        }
        return newFixedLengthResponse("This request isn't a POST");
    }


    public Boolean checkToken() {
        if (token == null){
            return false;
        }

        // IF Received time + expires_in seconds > current time THEN get new token
        try {
            Date now = new Date();
            Date expiration = new Date();
            /*
             Multiplying by 500 instead of 1000 to get 1/2 expiry time
             this way we renew early
            */
            expiration.setTime(received.getTime() + token.getInt("expires_in") * 500L);
            // 0 is equal, greater than 1 is after, less than 1 is before
            if(expiration.compareTo(now) >= 1)
            {
                Log.w("ProxyServer", "Token is still good");
                return true;
            }
            if(expiration.compareTo(now) <= 0)
            {
                Log.w("ProxyServer", "Time to renew");
                return false;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.w("ProxyServer", "Unable to find a token: " + e);
        }
        return false;
    }


    public void getToken() {
        /*
         Log.w("ProxyServer", "ClientId: " + clientId);
         Log.w("ProxyServer", "ClientSecret: " + clientSecret);
         Log.w("ProxyServer","TenantId: " + tenantId);
        */
        try {
            SSLContext sslContext = SSLContexts.createDefault();

            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,
                    new String[]{"TLSv1.2"},
                    null,
                    new StrictHostnameVerifier());

            CloseableHttpClient client = HttpClients.custom()
                    .setSSLSocketFactory(sslsf)
                    .build();
            HttpPost httpPost = new HttpPost("https://login.microsoftonline.com/" + tenantId + "/oauth2/v2.0/token");
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("scope", "https://outlook.office365.com/.default"));
            params.add(new BasicNameValuePair("client_id", clientId));
            params.add(new BasicNameValuePair("client_secret", clientSecret));
            params.add(new BasicNameValuePair("grant_type", "client_credentials"));
            httpPost.setEntity(new UrlEncodedFormEntity(params));

            HttpResponse response = client.execute(httpPost);
            String json = EntityUtils.toString(response.getEntity(), "utf-8");
            token = new JSONObject(json.toString());
            received = new Date();
            Log.w("ProxyServer", "Got new token.)");
        } catch (Exception e) {
            Log.w("ProxyServer", "Error getting token: " + e.toString());
        }
    }


    public String sendDataToMicrosoft(String originalPostData) {
        try {
            SSLContext sslContext = SSLContexts.createDefault();

            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,
                    new String[]{"TLSv1", "TLSv1.1", "TLSv1.2"},
                    null,
                    new StrictHostnameVerifier());

            CloseableHttpClient client = HttpClients.custom()
                    .setSSLSocketFactory(sslsf)
                    .build();
            HttpPost httpPost = new HttpPost("https://outlook.office365.com/ews/exchange.asmx");

            httpPost.setHeader("Authorization", "Bearer " +  token.getString("access_token"));
            httpPost.setHeader("Accept", "text/xml");
            httpPost.setHeader("Content-type", "text/xml");

            StringEntity data = new StringEntity(originalPostData);

            httpPost.setEntity(data);
            HttpResponse response = client.execute(httpPost);
            String microsoftResponse = EntityUtils.toString(response.getEntity(), "utf-8");
            Log.w("ProxyServer", "Response from microsoft: " + microsoftResponse);
            return microsoftResponse;
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("ProxyServer", "Error: " + e.toString());
        }
        return "";
    }


    public String[] parseBasicAuth(String authorization) {
        try {
            // Authorization: Basic base64credentials
            String base64Credentials = authorization.substring("Basic".length()).trim();
            byte[] credDecoded = android.util.Base64.decode(base64Credentials, android.util.Base64.DEFAULT);
            String credentials = new String(credDecoded, "utf-8");
            // credentials = username:password
            String[] values = credentials.split(":", 2);
            Log.w("ProxyServer", credentials);
            return values;
        } catch (Exception e) {
            Log.w("ProxyServer", "Unable to get username and password from authorization: " + e.toString());
            return new String[]{"none","none"};
        }
    }


    public NanoHTTPD.Response create401() {
        NanoHTTPD.Response response = NanoHTTPD.newFixedLengthResponse(NanoHTTPD.Response.Status.UNAUTHORIZED, "text/html", "Unauthorized");
        response.addHeader("WWW-Authenticate", "Basic realm=\"\"");
        return response;
    }
}