package com.example.android_ews_proxy;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import java.io.IOException;

public class MainActivity extends Activity {
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView home = (TextView) findViewById(R.id.title);
        home.setText("Hello EWS Proxy!");

        ProxyServer server = new ProxyServer();
        Log.w("ProxyServer", "Starting");
        try {
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.moveTaskToBack(true);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Log.w("ProxyServer", "Running in background");
    }
}